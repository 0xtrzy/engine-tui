#define _GNU_SOURCE
#include <stdlib.h> /* exit */
#include <string.h>
#include <stdio.h>

#include <locale.h>
#include <wctype.h>
#include <wchar.h>

#include <SDL2/SDL.h>
#include "rencache.h"

#ifdef _WIN32
 #include <windows.h>
#elif __linux__
// #include <unistd.h>
#elif __APPLE__
 #include <mach-o/dyld.h>
#endif

static double get_scale(void)
{
float dpi;
SDL_GetDisplayDPI(0, NULL, &dpi, NULL);
#if _WIN32
 return dpi / 96.0;
#elif __APPLE__
 return 1.0; /* dpi / 72.0; */
#else
 return 1.0;
#endif
}

static double get_scale(void);
