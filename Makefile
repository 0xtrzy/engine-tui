CC       = gcc
GLFLAG   = -lGL
OPCJESDL = `sdl2-config --libs` $(GLFLAG) -lm
OPCJE    = -W -Wall -O0 -std=c11 -pedantic -pedantic-errors

OBJ      = obj/
SRC      = ./src/
SRCSTB   = ./src/stb/
PROG     = test

all: $(PROG)

$(OBJ)%.o: $(SRC)%.c $(SRC)%.h
	@mkdir -p obj
	$(CC) $(OPCJE) -c -I$(SRC) -I$(SRCSTB) $< -o $@

$(OBJ)main.o: main.c main.h
	@mkdir -p obj
	$(CC) $(OPCJE) -c -I$(SRC) -I$(SRCSTB) $< -o $@

$(OBJ)stb_truetype.o: src/stb/stb_truetype.c Makefile
	$(CC) $(OPCJE) -c -Isrc src/stb/stb_truetype.c -o $(OBJ)stb_truetype.o

$(PROG): $(OBJ)main.o $(OBJ)stb_truetype.o $(OBJ)renderer.o $(OBJ)rencache.o
	rm -f ./$(PROG)
	$(CC) $(OPCJE) $^ -Isrc/stb -o $@ $(OPCJESDL)

mem: ./$(PROG)
	valgrind --leak-check=full --show-reachable=yes ./$(PROG)

.PHONY: clean

clean:
	rm -f ./*.o $(OBJ)*.o ./$(PROG)
