#include "main.h"

SDL_Window   *window;
SDL_Renderer *renderer;
SDL_Event     zd;
SDL_DisplayMode dm;

RenFont  *foncik;
RenColor  kolorek = {255, 204, 153, 255};
RenColor  czerwone = {0, 0, 255, 255};
RenRect   pr = {0, 0, 300, 300};
RenRect   prostokat = {100, 100, 50, 55};
RenRect   male = {10, 30, 10, 10};
SDL_Rect  rr;

int main()
{
int czy_konczymy = 0;

 grafika_start("test");
 ren_init(window);
 get_scale();

 renderer = SDL_CreateSoftwareRenderer(SDL_GetWindowSurface(window));
 SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
 SDL_RenderClear(renderer); /* czyszczenie kolorem */

 SDL_SetRenderDrawColor(renderer, 1, 9, 1, SDL_ALPHA_OPAQUE);
 SDL_RenderDrawLine(renderer, 5, 5, 100, 100);

 foncik = ren_load_font("zasoby/monospace.ttf", 18);
// w = ren_get_font_width(foncik,"Alleluja");
// printf("w=%d",w);
// w = ren_get_font_height(foncik);
// printf(" h=%d",w); fflush(stdout);

// ren_set_clip_rect(pr); nie potrzebne chyba

 ren_draw_rect(male, czerwone);
 ren_draw_text(foncik, "Alleluja ąż@", 14, 3, kolorek);
 ren_update_rects(&pr, 1);

/*
 rencache_show_debug(true);
// rencache_set_clip_rect(prostokat);
 rencache_begin_frame();
 rencache_draw_rect(male, czerwone);
// rencache_draw_text(foncik, "żółw", 14, 2, kolorek);
// rencache_free_font(foncik);
 rencache_end_frame();
*/

 SDL_RenderPresent(renderer);

 while (!czy_konczymy) /* główna pętla programu */
  {

  while (SDL_PollEvent(&zd))
   {

   if (zd.type == SDL_KEYDOWN) { czy_konczymy = 1; }

   if (SDL_PollEvent(&zd)) /* obsługujemy zdarzenia okien systemu operacyjnego */
    {
    switch (zd.type)
     {
     case SDL_QUIT: czy_konczymy = 1; break;
     }
    }

   } /* while zdarzenia */

  } /* while czy_konczymy */

 if (foncik) ren_free_font(foncik);
 if (renderer) { SDL_DestroyRenderer(renderer); }
 if (window) { SDL_DestroyWindow(window); }
 /* SDL_Quit(); niepotrzebne bo jest i tak uruchamiane */

return EXIT_SUCCESS;
}
